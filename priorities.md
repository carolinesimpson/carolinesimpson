# Weekly Priorities

## 2023-06-29 to 2023-06-23
1. [ ] Project Plan for remainder of CI Database Partitioning work.
1. [ ] Wittle down my TODO list.
1. [ ] Make team planning process documentation more consumable.
1. [ ] Build capacity planning model from data.


## 2023-05-08 to 2023-05-12
1. [ ] Project Plan for remainder of CI Database Partitioning work.
1. [x] Finish reviewing assigned MRs for MR audit.


## 2023-05-08 to 2023-05-12
1. [x] Update Q2 OKRs with information about how they will be measured.
1. [ ] Project Plan for remainder of CI Database Partitioning work.
1. [x] Ensuring all breaking changes are in for self-managed.

## 2023-05-01 to 2023-05-05 (Verify Async Week)
1. [ ] Update Q2 OKRs with information about how they will be measured.
1. [ ] Project Plan for remainder of CI Database Partitioning work.
1. [x] Focus on getting through TODO list items while we are working async this week.


## 2023-04-24 to 2023-04-28
1. [X] Review / update kickoff for role opening
1. [X] Finalize Q2 OKRs (create in GitLab)
1. [X] Merge FY24 Technical Roadmap onto team page
1. [ ] Project Plan for remainder of CI Database Partitioning work

## 2023-04-17 to 2023-04-21
1. [x] Draft Q2 OKRs
1. [x] Get feedback on FY24 Technical Roadmap
1. [x] Ensure breaking changes / removals are assigned for 16.0

## 2023-04-03 to 2023-04-07
Short week for me with PTO on Monday and Friday
1. [x] Complete equity refresh conversations
1. [x] Work on analysis of CI Data Partitioning process https://gitlab.com/gitlab-org/verify-stage/-/issues/417
1. [x] Work on technical roadmap for FY24 https://gitlab.com/gitlab-org/verify-stage/-/issues/405

## 2023-03-27 to 2023-03-31
1. [ ] Finish any learning to get up to speed on the CI Data Partitioning project / look at way to measure progress effectively. - In progress
1. [x] Work on analysis of CI Data Partitioning process https://gitlab.com/gitlab-org/verify-stage/-/issues/417 - Started
1. [x] Complete for quarterly career development conversations with team members
1. [x] Work on technical roadmap for FY24 https://gitlab.com/gitlab-org/verify-stage/-/issues/405 - Started

## 2023-03-20 to 2023-03-24
Async week + short week (F&F day + 1 PTO day)
1. [x] Complete Reading/Watching for IMOC training
1. [~] Complete for quarterly career development conversations with team members - deferred to have sync meeting
1. [ ] Focus on getting through TODO list items - Partially Complete

## 2023-03-13 to 2023-03-17
1. [ ] Complete Reading/Watching for IMOC training - Partially
1. [~] Complete for quarterly career development conversations with team members - Partially - 1 postponed
1. [ ] Finish any learning to get up to speed on the CI Data Partitioning project / look at way to measure progress effectively

## 2023-03-06 to 2023-03-10
1. Do IMOC training - Y (Partially complete)
1. Ensure transition plan is created for incoming/outgoing categories on the engineering side - Y
1. Prepare for quarterly career development conversations with team members (scheduled for next week) - Y (Partially Complete)
1. Finish any learning to get up to speed on the CI Data Partitioning project / look at way to measure progress effectively - N

## 2023-02-13 to 2023-02-17
1. Finish any learning to get up to speed on the CI Data Partitioning project - N
1. Ensure coverage issue ready for my PTO next week - Y
1. Continue deep dive into milestone planning/throughput. https://gitlab.com/gitlab-org/verify-stage/-/issues/363 - N
1. Do IMOC training - N

## 2023-02-06 to 2023-02-10
1. Do IMOC training. - N
1. Understand the Compute Credit Limits per project and sub-group epic well enough to speak in the community office hours about it. https://gitlab.com/groups/gitlab-org/-/epics/6378 - Y
1. Finish any learning to get up to speed on the CI Data Partitioning project - N (Partially)
1. Continue deep dive into milestone planning/throughput. https://gitlab.com/gitlab-org/verify-stage/-/issues/363 - N

## 2023-01-30 to 2023-02-03
1. Create OKRs for Pipeline Execution in GitLab. 
1. Get up to speed on the CI Data Partitioning project to take over for Scott
1. Continue deep dive into milestone planning/throughput. https://gitlab.com/gitlab-org/verify-stage/-/issues/363

## 2023-01-23 to 2023-01-27
1. Ensure the 15.9 milestone has an attainable amount of maintenance and bug issues and Deliverable and Stretch labels are set accordingly. 
1. Plan / Complete follow-up items for [Team-Ops Trainer training](https://gitlab.com/gitlab-com/ceo-chief-of-staff-team/cos-team/-/issues/305) - Y
1. Identify short term wins based on gaps with competition. https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/133 - Y
1. Start deep dive into milestone planning/throughput. https://gitlab.com/gitlab-org/verify-stage/-/issues/363 - Y / Started, but by no means complete

## 2023-01-16 to 2023-01-20
1. Start deep dive into milestone planning/throughput. https://gitlab.com/gitlab-org/verify-stage/-/issues/363 - N
1. Finish planning [Q4 socials](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/130) / setup plan for ongoing [Verify socials](https://gitlab.com/gitlab-org/verify-stage/-/issues/356). - Y
1. Plan / Complete follow-up items for [Team-Ops Trainer training](https://gitlab.com/gitlab-com/ceo-chief-of-staff-team/cos-team/-/issues/305) - N / Partially

## 2023-01-09 to 2023-01-13
1. Learn about our Annual compensation Review process and what is required - Y
1. Finish Talent Assessment meetings with team members. - Y
1. Complete outstanding items for [Team-Ops Trainer training](https://gitlab.com/gitlab-com/ceo-chief-of-staff-team/cos-team/-/issues/305) and [Activate Your Agile Career training](https://about.gitlab.com/company/culture/inclusion/tmrg-gitlab-women/agile-career/). - Y
1. Start deep dive into milestone planning/throughput. https://gitlab.com/gitlab-org/verify-stage/-/issues/363 - N
1. Finish planning [Q4 socials](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/130) / setup plan for ongoing [Verify socials](https://gitlab.com/gitlab-org/verify-stage/-/issues/356). - N


## 2023-01-03 to 2023-01-06
(Short week due to New Years day observation)
1. Catch up on Todo's after 2 weeks off. - Y (mostly)
1. Start deep dive into milestone planning/throughput. https://gitlab.com/gitlab-org/verify-stage/-/issues/363 - N
1. Schedule Talent Assessment meetings with team members.  - Y
1. Finish planning [Q4 socials](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/130) / setup plan for ongoing [Verify socials](https://gitlab.com/gitlab-org/verify-stage/-/issues/356). - N


## 2022-12-12 to 2022-12-16
1. Finish Talent Assessment written portions and updating Workday. - Y
1. Plan [Q4 socials](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/130) / setup plan for ongoing [Verify socials](https://gitlab.com/gitlab-org/verify-stage/-/issues/356). - N / Partially
1. Start deep dive into milestone planning/throughput. https://gitlab.com/gitlab-org/verify-stage/-/issues/363 - N

## 2022-12-05 to 2022-12-09
Continuing with the same priorities as last week as I didn't finish the items.
1. Finish Talent Assessment written portions.
1. Finish as much as my own onboarding training as possible that I've been deprioritizing.

## 2022-11-28 to 2022-12-02
Continuing with the same priorities as last week as I didn't finish the items.
1. Finish Talent Assessment written portions.
1. Finish as much as my own onboarding training as possible that I've been deprioritizing.
1. Ensuring we are set up to complete https://gitlab.com/gitlab-org/gitlab/-/issues/335465 for 16.0

## 2022-11-21 to 2022-11-25
1. Finish Talent Assessment written portions. - N (partial)
1. Finish as much as my own onboarding training as possible that I've been deprioritizing. - N (partial)


## 2022-11-14 to 2022-11-18
1. Ensure new team member is all set. - Y
1. Get our upcoming milestones into shape (weights applied, issues quantities, priorities) - Y
1. Finish as much as my own onboarding training as possible that I've been deprioritizing - N

Note: Short week - Friday is a Friends & Family Day.

## 2022-11-07 to 2022-11-11
1. OKR setup - Y
1. Error Budget - we've gone red over the last week 😮 - Y (Partially)
1. Prepare for new team member - Y

### Bonus
1. Complete Iteration Training (https://gitlab.com/gitlab-com/Product/-/issues/4876) - N
1. Complete Feature Flag Training (https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/13981) - N

## 2022-10-31 to 2022-11-04
1. Complete Talent Assessments - my reports and my own - Parially (the required parts)
1. Ensure everything is set up for new team members (Both for FEs and new hire.) - Partially
1. Complete Iteration Training (https://gitlab.com/gitlab-com/Product/-/issues/4876) - N

### Bonus
1. Complete Feature Flag Training (https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/13981) - N

## 2022-10-24 to 2022-10-28
1. Talent Assessments - my reports and my own - N (progress made)
1. Dashboard for Milestone Planning Insights (WIP - https://app.periscopedata.com/app/gitlab/1068424/WIP---Verify-Milestone-Planning-Exploration) - N (progress made)
1. Hiring - anything needed through offer approval process - Y

### Bonus
1. Update PE Weekly Async Update template to reflect the latest iteration - Y

## 2022-10-17 to 2022-10-21
1. Hiring - Y
1. Talent Assessments - my reports and my own - N
1. Catching up after PTO - Y

### Bonus
1. Update PE Weekly Async Update template to reflect the latest iteration - N
1. Complete Feature Flag Training (https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/13981) - N


# Ongoing Goals

- Investigate and address the ongoing disconnect between issues planned for milestones versus issues completed for milestones in PE.
   - Understand what an expected carryover rate is (if any).
- Complete onboarding training


# Ideas to explore
- Look into [Fast Boot](https://about.gitlab.com/handbook/engineering/fast-boot/) gathering and whether it is a good fit for our team.

