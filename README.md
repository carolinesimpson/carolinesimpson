## Caroline Simpson's README

**Caroline Simpson, Engineering Manager - Verify:Pipeline Execution**

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Related pages

  * [Group Team](https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-execution/)
  * [Personal Blog/Website](https://carolinesimpson.ca/)
  * [LinkedIn](https://www.linkedin.com/in/caroline-simpson-6703ab5/)
  * [Little-used Twitter](https://twitter.com/cpsimpson)
  * [Instagram](https://www.instagram.com/__simply_caroline__/)


## About me

I've worked in the software industry for 20 or so years in various different roles. I spent the majority of my career working as a developer. Python is my language of choice generally, but I try not to judge other ones too harshly. They each have their benefits and drawbacks. 

### Outside of Work 

I am currently working on a second degree at the University of Waterloo. I'm doing a Bachelor of Arts in Psychology currently. I am interested in how people think, communicate, and develop. Each new area of psychology that I explore leads me to new paths I want to go down. 

I also play bass in a band. We aren't particularly good, but we have fun trying. 

## Core Values

  * Honesty
  * Caring

## How you can help me

  * Let me know if you need/want something from me and aren't getting it.
  * Remind me if it seems like I've forgotten about a request you've made, it is entirely possible I have.


## What I want to earn
  
  * A greater understanding of the Verify stage of GitLab as I'm still fairly new and getting acquainted with the details of what we do.
  * The trust and respect of fellow team members.
  * Greater skills as a people manager. 

## Communicating with me

* I prefer honest, open communication
   * Even if something might seem hard or upsetting, honesty and clarity result in better outcomes.
* I like aynchronous when possible, so that I have time to process the message and respond thoughtfully.
* There is a good chance I will respond at random times of day depending on what else I'm doing. 
   * I don't expect this of others; I know it isn't a great habit.
   * If I am busy with something else, I will get back to you as soon as I am available. 
   * I make heavy use of slack reminders so I don't loose track of messages.
